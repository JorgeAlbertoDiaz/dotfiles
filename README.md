# dotfiles

Este proyecto agrega o modifica archivos de configuración a los dotfiles del proyecto **dotfiles** de **stephan raabe**, el cual incluye una configuración de escritorios de mosaico para `hyperland` y para `qtile`.

## Requerimientos
- Tener instalada y actualiza una distro basada en Arch Linux
- Instalar y tener en funcionamiento los archivos de configuración del proyecto [ML4W](https://gitlab.com/stephan-raabe/dotfiles)
