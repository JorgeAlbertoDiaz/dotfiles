#!/bin/bash
version=$(cat .version/name)
source .install/includes/colors.sh
source .install/includes/library.sh
clear

# Estable el modo de instalación
mode="live"
if [ ! -z $1 ]; then
    mode="dev"
    echo "IMPORTANT: MODO DEV ACTIVADO. "
    echo "La carpeta dotfiles existente no será modificada."
    echo "Links simbólicos no serán creados."
fi
echo -e "${GREEN}"
cat <<"EOF"
      _       _    __ _ _           
   __| | ___ | |_ / _(_) | ___  ___ 
  / _` |/ _ \| __| |_| | |/ _ \/ __|
 | (_| | (_) | |_|  _| | |  __/\__ \
  \__,_|\___/ \__|_| |_|_|\___||___/
                                                                 
EOF
echo -e "${NONE}"

# echo "Versión: $version"
echo "por Jorge A. Díaz"
echo ""
if [ -d ~/dotfiles ] ;then
    echo "Se detecto una instalación de los dotfiles de ML4W."
    echo "Este script sobreescribirá algunos archivos de configuración."
else
    echo "Debes instalar los dotfiles del proyecto ML4W."
fi
echo ""

source .install/confirm-start.sh
source .install/installer.sh
source .install/packages/custom-packages.sh
source .install/install-packages.sh
source .install/profile.sh

if [[ $profile == *"PHP"* ]]; then
    echo -e "${GREEN}"
    figlet "PHP"
    echo -e "${NONE}"
    source .install/packages/php-packages.sh
    source .install/install-packages.sh
fi

