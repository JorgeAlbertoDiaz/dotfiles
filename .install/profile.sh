# ------------------------------------------------------
# Selecciona un perfil de instalación
# ------------------------------------------------------
echo -e "${GREEN}"
figlet "Perfiles"
echo -e "${NONE}"

echo "ESPACIO = selecciona/deselecciona un perfil. ENTER = confirmar. Sin selección= CANCELAR"
profile=$(gum choose --no-limit --cursor-prefix "( ) " --selected-prefix "(x) " --unselected-prefix "( ) " "MariaDB" "PHP")

if [ -z "${profile}" ] ;then
    echo "Ningún perfil seleccionado. Instalación cancelada."
    exit
else
    echo "Perfiles/s seleccionado/s:" $profile
fi


