# ------------------------------------------------------
# Confirma el inicio
# ------------------------------------------------------

echo "Puedes cancelar la instalación en cualquier momento presionando CTRL + C"
SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
if [ $SCRIPTPATH = "/home/$USER/dotfiles" ]; then
    echo ""
    echo "IMPORTANTE: Esta corriendo el script de instalación desde el directorio de instalación."
    echo "Por favor mueve la carpeta de instalación desde otra ruta por ejemplo a ~/Downloads/ e inicia el script otra vez."
    echo "No es recomendable continuar con el procedimiento!"
    if [ ! $mode == "dev" ]; then
        exit
    fi
fi
if [ ! -d ~/dotfiles ];then
    if gum confirm "¿DESEAS INICIAR LA INSTALACIÓN DE LOS DOTFILES?" ;then
        echo "Intalación iniciada."
    elif [ $? -eq 130 ]; then
            exit 130
    else
        echo ":: Instalación cancelada."
        exit;
    fi
else
    if gum confirm "¿DESEAS INICIAR EL PROCESO DE ACTUALIZACIÓN DE LOS ARCHIVOS?" ;then
        echo ":: Actualización iniciada."
    elif [ $? -eq 130 ]; then
            exit 130
    else
        echo ":: Actualización cancelada."
        exit;
    fi
fi
echo ""

