packagesPacman=(
    "git"
    "htop"
    "libreoffice-still"
    "nmap"
    "speedtest-cli"
    "tmux"
);

packagesYay=(
    "dbeaver"
    "firefox" 
    "google-chrome"
    "insomnia"
    "visual-studio-code-bin"
);

