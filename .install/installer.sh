# ------------------------------------------------------
# Installa los paquetes requeridos
# ------------------------------------------------------
echo -e "${GREEN}"
figlet "Packages"
echo -e "${NONE}"
if [ -d ~/dotfiles ] ;then
    echo "¿Quieres instalar solo los paquetes nuevos? (instalación más rápida)"
    echo "o ¿Prefieres reinstalar todos los paquetes otra vez? (más robusta y puede ayudar a reparar problemas)"
    if gum confirm "¿Cómo deseas proceder??" --affirmative "Solo paquetes nuevos" --negative "Forzar reinstalación" ;then
        force_install=0
    elif [ $? -eq 130 ]; then
        echo "Instalación cancelada."
        exit 130
    else
        force_install=1
    fi
else
    echo "¿Deseas reinstalar todos los paquetes ya instalados e instalar los paquetes requeridos? (recomendado y más robusto)"
    echo "o ¿Quieres instalar solo los paquetes nuevos? (instalación más rápida)"
    if gum confirm "¿Cómo deseas proceder?" --affirmative "Reinstalar todos los paquetes" --negative "Intalar solo los nuevos paquetes" ;then
        force_install=1
    elif [ $? -eq 130 ]; then
        echo "Instalación cancelada."
        exit 130
    else
        force_install=0
    fi
fi

